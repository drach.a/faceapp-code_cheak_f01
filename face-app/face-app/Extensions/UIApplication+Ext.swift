//
//  UIApplication+Ext.swift
//  face-app
//
//  Created by Aliona Kostenko on 29.06.2021.
//

import UIKit

extension UIApplication {
    
    var safeAreaInsets: UIEdgeInsets {
        windows.first?.safeAreaInsets ?? .zero
    }
}
public extension String {
    func setColor(_ color: UIColor, ofSubstring substring: String) -> NSMutableAttributedString {
        let range = (self as NSString).range(of: substring)
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        return attributedString
    }
    
    func setFont(_ font : UIFont,ofSubstring substring: String ) -> NSMutableAttributedString  {
        let range = (self as NSString).range(of: substring)
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttribute(NSAttributedString.Key.font, value: font, range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
        return attributedString
    }
}
extension UIApplication {

    func setRootVC(_ vc : UIViewController){

        self.windows.first?.rootViewController = vc
        self.windows.first?.makeKeyAndVisible()

      }
  }

