//
//  NibInstantiatable.swift
//  face-app
//
//  Created by Aliona Kostenko on 29.06.2021.
//

import UIKit

public protocol NibInstantiatable {
    static func nibName() -> String
}

extension NibInstantiatable {
    
    static func nibName() -> String {
        return String(describing: self).replacingOccurrences(of: "<.*?>", with: "", options: .regularExpression)
    }
    
}

extension NibInstantiatable where Self: UIView {
    
    func fromNib() -> UIView {
        let bundle = Bundle(for: Self.self)
        let nib = bundle.loadNibNamed(Self.nibName(), owner: self, options: nil)
        return nib!.first as! UIView
    }
}

extension NibInstantiatable where Self: UIViewController {
    
    static func fromNib() -> Self {
        return self.init(nibName: nibName(), bundle: Bundle(for: self))
    }
}
