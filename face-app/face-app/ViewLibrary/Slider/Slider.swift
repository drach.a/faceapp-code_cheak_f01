//
//  Slider.swift
//  face-app
//
//  Created by Aliona Kostenko on 30.06.2021.
//

import UIKit
import Combine

class Slider: UIView, NibInstantiatable {
    
    var onFinish: ((_ saveEditing: Bool) -> Void)?

    @IBOutlet weak var slider: UISlider!
    
    var contentView: UIView?
    let sliderValueSubject: PassthroughSubject<NSNumber, Never> = .init()
    
    @IBAction func sliderValueDidChange(_ sender: UISlider) {
        sliderValueSubject.send(NSNumber(value: Double(sender.value)))
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        let view = fromNib()
        view.frame = bounds
        addSubview(view)
        contentView?.addSubview(view)
    }
    
    func setRange(range: ClosedRange<Double>) {
        slider.minimumValue = Float(range.lowerBound)
        slider.maximumValue = Float(range.upperBound)
    }
    
    func setCurrentValue(value: Double) {
        slider.value = Float(value)
    }

}
