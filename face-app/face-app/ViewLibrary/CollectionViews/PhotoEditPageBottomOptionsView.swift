//
//  PhotoEditPageBottomOptionsView.swift
//  face-app
//
//  Created by Aliona Kostenko on 29.06.2021.
//

import UIKit

@IBDesignable
class PhotoEditPageBottomOptionsView: UIView, NibInstantiatable,
                                      UICollectionViewDataSource,
                                      UICollectionViewDelegate,
                                      UICollectionViewDelegateFlowLayout {
    var model: [PhotoEditOption] = []
    
    var contentView: UIView?

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var onSelectOption: ((PhotoEditOption) -> Void)?
    
    public func onSelectOption(handler: @escaping (PhotoEditOption) -> Void) {
        onSelectOption = handler
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        let view = fromNib()
        view.frame = bounds
        addSubview(view)
        contentView = view
        setupCollectionViewLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // spread items in collection if size wider than all items
        if bounds.width > itemWidth * CGFloat(model.count) {
            setSpreadLayout()
        }
    }
    
    private func setupCollectionViewLayout() {
        collectionView.register(PhotoEditOptionCollectionViewCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsHorizontalScrollIndicator = false
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 24
        flowLayout.minimumInteritemSpacing = 24
        flowLayout.sectionInset = UIEdgeInsets(top: UIApplication.shared.safeAreaInsets.bottom / 2, left: 24, bottom: 0, right: 24)
        collectionView.setCollectionViewLayout(flowLayout, animated: true)
    }
    
    private func setSpreadLayout() {
        let cellsPerRow: CGFloat = CGFloat(model.count)
        let containerWidth: CGFloat = bounds.width
        let availableSpace = containerWidth - cellsPerRow * itemWidth
        let inset = availableSpace / (cellsPerRow + 1)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.minimumInteritemSpacing = inset
        layout.minimumLineSpacing = inset
        layout.sectionInset = UIEdgeInsets(top: UIApplication.shared.safeAreaInsets.bottom / 2, left: inset, bottom: 0, right: inset)
        layout.scrollDirection = .horizontal
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    // MARK: - Layout
    
    private var itemWidth: CGFloat {
        bounds.width * 0.15
    }
    
    private var itemHeight: CGFloat {
        (bounds.height - UIApplication.shared.safeAreaInsets.bottom) * 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = model[indexPath.item]
        onSelectOption?(item)
    }
    
    // MARK: - Data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PhotoEditOptionCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        let option = model[indexPath.item]
        cell.optionImageView.image = option.image
        cell.optionLabel.text = option.title
        return cell
    }
}

