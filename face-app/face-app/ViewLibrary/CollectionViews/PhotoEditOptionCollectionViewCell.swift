//
//  PhotoEditOptionCollectionViewCell.swift
//  face-app
//
//  Created by Aliona Kostenko on 29.06.2021.
//

import UIKit

class PhotoEditOptionCollectionViewCell: UICollectionViewCell, NibInstantiatable {

    @IBOutlet weak var optionImageView: UIImageView!
    @IBOutlet weak var optionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
