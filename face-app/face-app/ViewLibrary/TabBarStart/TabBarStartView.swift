//
//  TabBarStartView.swift
//  face-app
//
//  Created by Aliona Kostenko on 01.07.2021.
//

import UIKit

class TabBarStartView: UIView {

    @IBOutlet weak var addPhoto: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
        addPhoto.layer.cornerRadius = 32
        self.layer.shadowColor = #colorLiteral(red: 0.1180943921, green: 0.7880262733, blue: 0.8836199045, alpha: 0.3014279801)
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 10
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("TabBarStartView", owner: self, options: nil)! [0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    @IBAction func selectPhoto(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("selectPhoto"), object: nil)
    }
}
