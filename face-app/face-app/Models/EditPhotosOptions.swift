//
//  EditPhotosOptions.swift
//  face-app
//
//  Created by michael on 24.06.2021.
//

import UIKit

protocol PhotoEditOption {
    var title: String { get }
    var image: UIImage? { get }
}

enum EditPhotosMainOptions: CaseIterable, PhotoEditOption {
    case text
    case draw
    case effects
    case orientation
    case crop
    case lighting
    case color
    case vignette
    case blur
    
    var title: String {
        switch self {
        case .text: return "Text"
        case .draw: return "Draw"
        case .effects: return "Effects"
        case .orientation: return "Orientation"
        case .crop: return "Crop"
        case .lighting: return "Lighting"
        case .color: return "Color"
        case .vignette: return "Vignette"
        case .blur: return "Blur"
        }
    }
    var image: UIImage? {
        switch self {
        case .text: return UIImage(named: "text")
        case .draw: return UIImage(named: "draw")
        case .effects: return UIImage(named: "effects")
        case .orientation: return UIImage(named: "text")
        case .crop: return UIImage(named: "text")
        case .lighting: return UIImage(named: "text")
        case .color: return UIImage(named: "text")
        case .vignette: return UIImage(named: "text")
        case .blur: return UIImage(named: "text")
        }
    }
}
