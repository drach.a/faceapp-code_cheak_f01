//
//  TabBarCollectionViewCell.swift
//  face-app
//
//  Created by michael on 24.06.2021.
//

import UIKit

class TabBarCollectionViewCell: UICollectionViewCell {
    
    var imageButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.blue
        button.layer.cornerRadius = 18
        button.clipsToBounds = true
//        button.setImage(R.image.pencil(), for: .normal)

        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "draw")?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = UIColor.red
        return iv
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.blue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.text = "4.9+"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
