//
//  StartViewController.swift
//  face-app
//
//  Created by Aliona Kostenko on 30.06.2021.
//

import UIKit

class StartViewController: UIViewController {
   
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var headerv: HeaderView!
    @IBOutlet weak var tabBarV: TabBarStartView!
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(selectPhoto), name: Notification.Name("selectPhoto"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(photoEditorOpen), name: Notification.Name("photoEditorOpen"), object: nil)
        
        imagePicker.delegate = self
//        let header = HeaderView(frame: CGRect(x: 20.0, y: 100.0, width: 300.0, height: 300.0))
//        header.lblText.text = "bu"
//        self.view.addSubview(header)
//        headerv.lblText.text = "hi"
    }
    
    @objc func selectPhoto(notification: NSNotification) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func photoEditorOpen(notification: NSNotification) {
        let photoEditView = PhotoEditorViewController(nibName: "PhotoEditorViewController", bundle: nil)
        photoEditView.modalPresentationStyle = .fullScreen
        self.present(photoEditView, animated: true, completion: nil)
       }
}

extension StartViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        if let image = info[.editedImage] as? UIImage {
            selectedImage.image = image
        }
        dismiss(animated: true, completion: nil)
    }
}
