//
//  PhotoEditorNavbar.swift
//  face-app
//
//  Created by michael on 24.06.2021.
//

import UIKit

class PhotoEditorNavbar {
    
    private var cancelCallback: (() -> Void)?
    private var confirmCallback: (() -> Void)?
    
    private var cancelButton: UIButton {
        let cancel = UIButton(type: .custom)
        cancel.setTitle("Cancel", for: .normal)
        cancel.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        cancel.addTarget(self, action: #selector(cancelEdit), for: .touchUpInside)
        return cancel
    }
    
    private var confirmButton: UIButton {
        let confirm = UIButton(type: .custom)
        confirm.setTitle("Done", for: .normal)
        confirm.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        confirm.addTarget(self, action: #selector(confirmEdit), for: .touchUpInside)
        return confirm
    }
    
    @objc private func cancelEdit() {
        cancelCallback?()
    }
    @objc private func confirmEdit() {
        confirmCallback?()
    }
}
